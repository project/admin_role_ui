INTRODUCTION
------------

The Admin Role UI module overrides Drupal core to provide alternative/enhanced
versions of the UI related to administrative roles and to bring it inline with
how the admin roles work in code, which is different to what's allowed in the
core UI.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/admin_role_ui

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/admin_role_ui

FEATURES
--------

* Disables the core "Administrator role" settings form in order to prevent
  admins from [inadvertently locking themselves out](1).

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

This module has no menu or modifiable settings. There is no configuration. When
enabled, the module will disable the core administrator roles form. To restore
the core functionality, disable the module and clear caches.

[1]: https://www.drupal.org/project/drupal/issues/3293874
